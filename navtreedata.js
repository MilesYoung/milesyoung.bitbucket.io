/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405: Mechatronics Portfolio", "index.html", [
    [ "ME 405: Mechatronics", "index.html", [
      [ "Introduction", "index.html#sec_intro", null ],
      [ "Vendotron", "index.html#sec_Vend", null ],
      [ "Reaction Time Test", "index.html#sec_rxntime", null ],
      [ "Button Voltage Response", "index.html#sec_btnvolt", null ],
      [ "Temperature Measurement", "index.html#sec_tempsense", null ],
      [ "Touch Panel Driver", "index.html#sec_touchPanel", null ],
      [ "Updated Motor and Encoder Drivers", "index.html#sec_motorencoder", null ]
    ] ],
    [ "BalanceBot Dynamic Analysis", "page1.html", [
      [ "Hand Calculations", "page1.html#sec_handcalcs", null ]
    ] ],
    [ "BalanceBot Dynamic Simulation", "page2.html", [
      [ "Linearizing Model", "page2.html#sec_lin", null ],
      [ "Simulating Open Loop System", "page2.html#sec_OLplot", null ],
      [ "Simulating Closed Loop System", "page2.html#sec_CLplot", null ]
    ] ],
    [ "BalanceBot Term Project", "page3.html", [
      [ "Background on Balance Board", "page3.html#Intro_sec", null ],
      [ "Hardware", "page3.html#hardware_sec", null ],
      [ "Main, Driver, and Task Programs", "page3.html#Drivers_sec", null ],
      [ "BNO055 IMU Sensor", "page3.html#bno055_sec", null ],
      [ "Controller", "page3.html#Controller_sec", null ],
      [ "Tuning the Controller", "page3.html#tuning_sec", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"ButtonUI_8py.html",
"main__young_8py.html#a6a17ac05d21b322e53d0c883ff94e4a1"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';