var files_dup =
[
    [ "Button_main.py", "Button__main_8py.html", "Button__main_8py" ],
    [ "ButtonUI.py", "ButtonUI_8py.html", "ButtonUI_8py" ],
    [ "CLDriver.py", "CLDriver_8py.html", [
      [ "CLDriver", "classCLDriver_1_1CLDriver.html", "classCLDriver_1_1CLDriver" ]
    ] ],
    [ "CLTask.py", "CLTask_8py.html", [
      [ "CLTask", "classCLTask_1_1CLTask.html", "classCLTask_1_1CLTask" ]
    ] ],
    [ "EncoderDriver.py", "EncoderDriver_8py.html", [
      [ "EncoderDriver", "classEncoderDriver_1_1EncoderDriver.html", "classEncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main_young.py", "main__young_8py.html", "main__young_8py" ],
    [ "mcp9808_young.py", "mcp9808__young_8py.html", "mcp9808__young_8py" ],
    [ "MotorDriver.py", "MotorDriver_8py.html", [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "ReactionTest.py", "ReactionTest_8py.html", "ReactionTest_8py" ],
    [ "TouchDriver.py", "TouchDriver_8py.html", "TouchDriver_8py" ],
    [ "Vendotron.py", "Vendotron_8py.html", "Vendotron_8py" ]
];