var classCLTask_1_1CLTask =
[
    [ "__init__", "classCLTask_1_1CLTask.html#acc18457b116782fa8e818deff88db2a6", null ],
    [ "run", "classCLTask_1_1CLTask.html#a7d65ecb091a274b882bb48f88bbc778d", null ],
    [ "transitionTo", "classCLTask_1_1CLTask.html#ac8621f40eb10625cd428048389919f52", null ],
    [ "angleRatio", "classCLTask_1_1CLTask.html#a5e10ee37fbead5bbba9d74bd6ea06010", null ],
    [ "ball_paramX", "classCLTask_1_1CLTask.html#afe201695f91ad1887edd729556faa61b", null ],
    [ "ball_paramY", "classCLTask_1_1CLTask.html#ab6619be36e25be4fcb64dd51b9522c3e", null ],
    [ "ball_rest", "classCLTask_1_1CLTask.html#a3059b4a95206304898f43b4d6d2be2e8", null ],
    [ "CL1", "classCLTask_1_1CLTask.html#acda31e1e3f73a74d3425b70772eaf9c9", null ],
    [ "CL2", "classCLTask_1_1CLTask.html#ac10777957f4f7bb1bb6cf6419c55a815", null ],
    [ "current_ball_pos", "classCLTask_1_1CLTask.html#af51b4177312fc410bd3cb8caecf91041", null ],
    [ "currTime", "classCLTask_1_1CLTask.html#a7722755172b0818eb27443edc8ca7307", null ],
    [ "dbg", "classCLTask_1_1CLTask.html#a79b50a6f16338dfb08bff0c4de6bfa3a", null ],
    [ "dutyArrayX", "classCLTask_1_1CLTask.html#af2ed1a1c2eb7ddd35945ca81693a2319", null ],
    [ "dutyArrayY", "classCLTask_1_1CLTask.html#aa848e9ee9f7dd3a7661b5f32356d04a9", null ],
    [ "Encoder1", "classCLTask_1_1CLTask.html#adbaa9382dfa462df3d0661aef70b145a", null ],
    [ "Encoder2", "classCLTask_1_1CLTask.html#a8c764f4036eb9618b6067c777943f515", null ],
    [ "InputTx", "classCLTask_1_1CLTask.html#ab6cfaee08247cbedb9cd8b857c31bfb8", null ],
    [ "InputTy", "classCLTask_1_1CLTask.html#abef52b210ae9fce3c874d6e5ba77651a", null ],
    [ "interval", "classCLTask_1_1CLTask.html#a8b5156b579d3310d4f0b547b622cbb54", null ],
    [ "last_ball_pos", "classCLTask_1_1CLTask.html#a78a7d1e824e580ae60043ed8c0bb7873", null ],
    [ "lp", "classCLTask_1_1CLTask.html#acc7f58143dac37ef204d5053eb457acf", null ],
    [ "Motor1", "classCLTask_1_1CLTask.html#a77aaee8eecd3797e3d8dc9dfd2a950c0", null ],
    [ "Motor2", "classCLTask_1_1CLTask.html#abaa24149b734e5c892f52f61b54da3a5", null ],
    [ "Motorx_feed", "classCLTask_1_1CLTask.html#a6a41b51322aaecff88c012ac6cd32fe3", null ],
    [ "Motory_feed", "classCLTask_1_1CLTask.html#a37e34290ced7b41b50c82653a8229f7a", null ],
    [ "nextTime", "classCLTask_1_1CLTask.html#ae96033c1fc01e3a3df074106b9d1c406", null ],
    [ "phi_x", "classCLTask_1_1CLTask.html#a7862a058b2108d0d5f330996e5249365", null ],
    [ "phi_x_dot", "classCLTask_1_1CLTask.html#a380718f2d17b2c781f83b3bb5fd28f27", null ],
    [ "phi_y", "classCLTask_1_1CLTask.html#a2e49db2d0a4b639bb978de99c9ded522", null ],
    [ "phi_y_dot", "classCLTask_1_1CLTask.html#afee88efd216e7181847cd9caaefaa1d4", null ],
    [ "plat_paramX", "classCLTask_1_1CLTask.html#aac667a444eefdc816e5733f2fc0ca87a", null ],
    [ "plat_paramY", "classCLTask_1_1CLTask.html#ae97490fa277d77a2c447dab172af6854", null ],
    [ "rm", "classCLTask_1_1CLTask.html#a34feb2998de82cf249652e39502c66c7", null ],
    [ "runs", "classCLTask_1_1CLTask.html#a7b3268a1b7ba93db881accc3dff97604", null ],
    [ "startTime", "classCLTask_1_1CLTask.html#a186a924db97912db0915bc73e2f53a14", null ],
    [ "state", "classCLTask_1_1CLTask.html#abbb804e7c2815ef758732136eb7131fc", null ],
    [ "theta_x", "classCLTask_1_1CLTask.html#a416a74e857c2d9ae4831d2996130c78d", null ],
    [ "theta_x_dot", "classCLTask_1_1CLTask.html#a2ac1e37bdd24f929e4bb92e0d1d7c803", null ],
    [ "theta_y", "classCLTask_1_1CLTask.html#a31feabf8354726ee6bf19b79ec8f7b51", null ],
    [ "theta_y_dot", "classCLTask_1_1CLTask.html#a20b250dc60fe3a1113a23d41fdeef5f4", null ],
    [ "timeArray", "classCLTask_1_1CLTask.html#a1387a2a65eb7162e4bbbf7fe201720e8", null ],
    [ "TouchObject", "classCLTask_1_1CLTask.html#a69d30c338aa512290eafcf01a93a3728", null ],
    [ "X_ball", "classCLTask_1_1CLTask.html#a2c0e4b9f0cd0f9094209f92f64ac062e", null ],
    [ "X_ball_dot", "classCLTask_1_1CLTask.html#ae64bc8d9562397f9dc6cd61aa261e270", null ],
    [ "Y_ball", "classCLTask_1_1CLTask.html#a1b01485193ffc9e92abf4eb80d1dd6d8", null ],
    [ "Y_ball_dot", "classCLTask_1_1CLTask.html#af49fd753577b2e64f27f78e2fc7b9ace", null ]
];