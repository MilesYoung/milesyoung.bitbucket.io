var classCLDriver_1_1CLDriver =
[
    [ "__init__", "classCLDriver_1_1CLDriver.html#aac59161b391bb390d3355af2790235f4", null ],
    [ "Controller", "classCLDriver_1_1CLDriver.html#a87c01343af816efdb9e499729265458e", null ],
    [ "TtoD", "classCLDriver_1_1CLDriver.html#ae15aa7b3403412c2a0ac6f4ed856fd4a", null ],
    [ "zero", "classCLDriver_1_1CLDriver.html#a3198c324144d06ac12c8a0f7981e871a", null ],
    [ "K1", "classCLDriver_1_1CLDriver.html#a6634f694c99153654ad0cd66e081a955", null ],
    [ "K2", "classCLDriver_1_1CLDriver.html#a220f5e395c24b5a8ee143eee1160676c", null ],
    [ "K3", "classCLDriver_1_1CLDriver.html#ace4b82ce54ccf1d5b4713a741f326b19", null ],
    [ "K4", "classCLDriver_1_1CLDriver.html#a53ef310c531f859d9679df9b921aef2f", null ],
    [ "Kt", "classCLDriver_1_1CLDriver.html#a0df52bca0887bfeac8e816c4d45e6ea9", null ],
    [ "resistance", "classCLDriver_1_1CLDriver.html#a03751c62889050c0601d94b6e556f527", null ],
    [ "Vdc", "classCLDriver_1_1CLDriver.html#a88c287fda0e9c19b01e141c9cb7cb80d", null ]
];