var annotated_dup =
[
    [ "CLDriver", null, [
      [ "CLDriver", "classCLDriver_1_1CLDriver.html", "classCLDriver_1_1CLDriver" ]
    ] ],
    [ "CLTask", null, [
      [ "CLTask", "classCLTask_1_1CLTask.html", "classCLTask_1_1CLTask" ]
    ] ],
    [ "EncoderDriver", null, [
      [ "EncoderDriver", "classEncoderDriver_1_1EncoderDriver.html", "classEncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "mcp9808_young", null, [
      [ "MCP9808", "classmcp9808__young_1_1MCP9808.html", "classmcp9808__young_1_1MCP9808" ]
    ] ],
    [ "MotorDriver", null, [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "TouchDriver", null, [
      [ "TouchDriver", "classTouchDriver_1_1TouchDriver.html", "classTouchDriver_1_1TouchDriver" ]
    ] ]
];